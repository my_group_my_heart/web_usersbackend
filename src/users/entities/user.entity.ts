export class User {
  [x: string]: any;
  id: number;
  login: string;
  name: string;
  password: string;
}
